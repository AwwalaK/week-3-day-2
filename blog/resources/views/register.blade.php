<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Register</title>
    </head>
    <body>
        <h1>Buat Account Baru!</h1>
	    <h3>Sign Up Form</h3>
        <form action="/submit">
            <p>
                <label>First name:</label><br>
                <input type="text">
            </p>
            <p>
                <label>Last name:</label><br>
                <input type="text">
            </p>
            <p>
            <p>Please select your gender:</p>
                <input type="radio" name="gender">Male<br>
                <input type="radio" name="gender">Female<br>
                <input type="radio" name="gender">Other<br>
            </p>
            <p>
                <label>Nationality:</label><br>
                <select>
                    <option>Indonesian</option>
                    <option>Inggris</option>
                    <option>Malaysian</option>
                </select>
            </p>
            <p>
                <label>Language Spoken:</label><br>
                <input type="checkbox">Bahasa Indonesia<br>
                <input type="checkbox">English<br>
                <input type="checkbox">Other</label><br>
            </p>
            <p>
                <label for="">Bio</label><br>
                <textarea cols="30" rows="10"></textarea>
            </p>
                <input type="Submit" value="Sign Up" active>

        </form>
    </body>
</html>